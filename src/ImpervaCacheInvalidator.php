<?php

namespace Drupal\imperva_cache_purger;

use Drupal\purge\Logger\LoggerChannelPartInterface;
use GuzzleHttp\Client;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Provides path and cache tags' invalidator for Imperva Cache.
 */
class ImpervaCacheInvalidator {

  /**
   * Indicates REST API endpoint for Imperva Cache.
   */
  const IMPERVA_API_ENDPOINT = 'https://my.imperva.com/api/prov/v2/sites/';

  /**
   * Http client for API connection.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Provides service object.
   *
   * @param \GuzzleHttp\Client $http_client
   *   Http client for API connection.
   */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * Invalidates caches.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   Config object.
   * @param null|\Drupal\purge\Logger\LoggerChannelPartInterface $logger
   *   Logger instance.
   * @param array $paths
   *   Array of paths that need to be invalidated.
   * @param array $tags
   *   Array of tags that need to be invalidated.
   */
  public function invalidate(ImmutableConfig $settings, ?LoggerChannelPartInterface $logger, array $paths = [], array $tags = []): void {
    $options = [];

    // Collect all headers.
    $options['headers'] = $this->getRequestHeaders($settings);

    // Execute purge request for each path as multiple paths are not supported
    // yet.
    if (!empty($paths) && ($settings->get('purger_type') == 'path_cache_tag' || $settings->get('purger_type') == 'path')) {
      foreach ($paths as $path) {
        $options['query']['url_pattern'] = $this->getRequestQuery($path);
        $this->executeRequest($options, $settings, $logger);
      }
    }

    // Executes purge request for cache tags.
    if (!empty($tags) && ($settings->get('purger_type') == 'path_cache_tag' || $settings->get('purger_type') == 'cache_tag')) {
      // Unset array 'url_pattern' element which was set above
      // before send a new request.
      unset($options['query']['url_pattern']);

      foreach ($tags as $tag) {
        $options['query']['tags'] = $this->getRequestQuery($tag);
        $this->executeRequest($options, $settings, $logger);
      }
    }
  }

  /**
   * Error handler for API requests.
   *
   * @param object $request
   *   Imperva API request.
   * @param null|\Drupal\purge\Logger\LoggerChannelPartInterface $logger
   *   Logger instance.
   */
  public function errorHandlerApi(object $request, ?LoggerChannelPartInterface $logger) {

    // Logs message from API to watchdog. Admins will have possibility to
    // find an issue.
    return $logger->error($request->getBody()->getContents());
  }

  /**
   * Prepares auth headers for request to API.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   Config object.
   *
   * @return array
   *   Array of headers.
   */
  public function getRequestHeaders(ImmutableConfig $settings): array {

    // x-api-key and x-api-id headers are used for authorization.
    return [
      'x-api-key' => $settings->get('api_key'),
      'x-api-id' => $settings->get('api_id'),
      'Content-Type' => 'application/json',
    ];
  }

  /**
   * Prepares query with params for API request.
   *
   * @param mixed $value
   *   Can be array of tags or path.
   */
  public function getRequestQuery($value) {

    // Purge request can be used for path or for cache tags.
    if (is_array($value)) {
      $value = implode(',', $value);
    }

    return $value;
  }

  /**
   * Prepares API Endpoint.
   *
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   Config object.
   *
   * @return string
   *   Endpoint path.
   */
  public function getApiEndpoint(ImmutableConfig $settings): string {
    // User can override Endpoint if is necessary to use different versions.
    $api_endpoint = self::IMPERVA_API_ENDPOINT;
    if ($settings->get('api_endpoint')) {
      $api_endpoint = $settings->get('api_endpoint');
    }

    return $api_endpoint . $settings->get('site_id') . '/cache';
  }

  /**
   * Executes request to API Endpoint of Imperva.
   *
   * @param array $options
   *   Array of settings for API request.
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   *   Config object.
   * @param null|\Drupal\purge\Logger\LoggerChannelPartInterface $logger
   *   Logger instance.
   */
  public function executeRequest(array $options, ImmutableConfig $settings, ?LoggerChannelPartInterface $logger) {

    $request = $this->httpClient->request('DELETE', $this->getAPIEndpoint($settings), $options);

    // If request was failed add info to watch dog.
    if ($request->getStatusCode() != 200) {
      return $this->errorHandlerAPI($request, $logger);
    }

    // Converts data from json to array.
    $data = $request->getBody()->getContents();

    if ($logger) {
      if (!empty($options['query']['url_pattern'])) {
        $logger->info('Successfully invalidated URL: @url', ['@url' => $options['query']['url_pattern']]);
      }
      if (!empty($options['query']['tags'])) {
        $logger->info('Successfully invalidated cache tags: @tags', ['@tags' => $options['query']['tags']]);
      }
    }

    // Returns id of log from imperva, for reference.
    $data = Json::decode($data);
    return $data['debug_info']['id-info'];
  }

}
