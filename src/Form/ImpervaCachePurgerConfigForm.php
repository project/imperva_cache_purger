<?php

namespace Drupal\imperva_cache_purger\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\purge_ui\Form\PurgerConfigFormBase;
use Drupal\imperva_cache_purger\ImpervaCacheInvalidator;

/**
 * Provides a config form for Imperva cache purger.
 */
class ImpervaCachePurgerConfigForm extends PurgerConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['imperva_cache_purger.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'imperva_cache_purger.config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('imperva_cache_purger.settings');
    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#description' => $this->t('Whether Imperva cache Purger is enabled.'),
      '#default_value' => empty($settings->get('disabled')),
    ];
    $form['purger_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Cache invalidation type'),
      '#options' => [
        'path_cache_tag' => $this->t('by path and cache tag'),
        'path' => $this->t('by path'),
        'cache_tag' => $this->t('by cache tag'),
      ],
      '#default_value' => $settings->get('purger_type') ?: 'path',
    ];
    $form['api_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API ID'),
      '#default_value' => $settings->get('api_id'),
      '#description' => $this->t('Check <a href="https://docs.imperva.com/bundle/cloud-application-security/page/settings/api-keys.htm">documentation</a> how to generate API ID.'),
    ];
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#default_value' => $settings->get('api_key'),
      '#description' => $this->t('Check <a href="https://docs.imperva.com/bundle/cloud-application-security/page/settings/api-keys.htm">documentation</a> how to generate API key.'),
    ];
    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site id'),
      '#default_value' => $settings->get('site_id'),
      '#description' => $this->t('Site id one can found on imperva dashboard -> Websites near site url. Example: 69763071'),
    ];
    $form['api_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API endpoint'),
      '#default_value' => $settings->get('api_endpoint') ?: ImpervaCacheInvalidator::IMPERVA_API_ENDPOINT,
      '#description' => $this->t('You can change endpoint in case of using different version of API, by default: %api', ['%api' => ImpervaCacheInvalidator::IMPERVA_API_ENDPOINT]),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormSuccess(array &$form, FormStateInterface $form_state) {
    $settings = $this->config('imperva_cache_purger.settings');
    $settings->set('disabled', empty($form_state->getValue('enabled')));
    $settings->set('purger_type', $form_state->getValue('purger_type'));
    $settings->set('api_id', $form_state->getValue('api_id'));
    $settings->set('api_key', $form_state->getValue('api_key'));
    $settings->set('site_id', $form_state->getValue('site_id'));
    $settings->set('api_endpoint', $form_state->getValue('api_endpoint'));
    $settings->save();
  }

}
