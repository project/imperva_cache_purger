<?php

namespace Drupal\imperva_cache_purger\Plugin\Purge\Purger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\purge\Plugin\Purge\Invalidation\EverythingInvalidation;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;
use Drupal\purge\Plugin\Purge\Invalidation\PathInvalidation;
use Drupal\purge\Plugin\Purge\Invalidation\WildcardPathInvalidation;
use Drupal\purge\Plugin\Purge\Invalidation\TagInvalidation;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\imperva_cache_purger\ImpervaCacheInvalidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Imperva cache Purger implementation.
 *
 * @PurgePurger(
 *   id = "imperva_cache_purger",
 *   label = @Translation("Imperva cache Purger"),
 *   configform = "\Drupal\imperva_cache_purger\Form\ImpervaCachePurgerConfigForm",
 *   cooldown_time = 0.0,
 *   description = @Translation("Uses Imperva API invalidations."),
 *   multi_instance = FALSE,
 *   types = {"path", "wildcardpath", "everything", "tag"},
 * )
 */
class ImpervaCachePurger extends PurgerBase {

  /**
   * The Imperva invalidator.
   *
   * @var \Drupal\imperva_cache_purger\ImpervaCacheInvalidator
   */
  protected $invalidator;

  /**
   * The settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * ImpervaCachePurger constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\imperva_cache_purger\ImpervaCacheInvalidator $invalidator
   *   The ImpervaCachePurger invalidator.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImpervaCacheInvalidator $invalidator, ConfigFactoryInterface $config_factory) {
    $this->invalidator = $invalidator;
    $this->settings = $config_factory->get('imperva_cache_purger.settings');

    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('imperva_cache_purger.invalidator'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    $paths = $tags = [];

    /** @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface $invalidation */
    foreach ($invalidations as $invalidation) {
      if ($invalidation instanceof EverythingInvalidation) {

        // Reset paths if we are invalidating everything.
        $paths = ['^/'];
        break;
      }
      elseif ($invalidation instanceof PathInvalidation || $invalidation instanceof WildcardPathInvalidation) {
        if (is_string($expression = $invalidation->getExpression())) {
          // Ensure we always have a leading slash.
          $paths[] = htmlentities('/' . ltrim($expression, ' /'));
        }
      }
      elseif ($invalidation instanceof TagInvalidation) {
        if (is_string($expression = $invalidation->getExpression())) {
          $tags[] = $expression;
        }
      }
      else {
        $invalidation->setState(InvalidationInterface::NOT_SUPPORTED);
      }
    }

    // Exit early if there are no paths and tags.
    if (empty($paths) && empty($tags)) {
      $this->logger()->info('No paths and tags found to purge');
      return;
    }

    if (!empty($this->settings->get('disabled'))) {
      // Allow some environments to swallow invalidations without error by
      // specifying an empty distribution.
      $this->logger()->info('Invalidations were ignored because Imperva cache Purger was disabled.');
      $this->setStates($invalidations, InvalidationInterface::SUCCEEDED);
      return;
    }

    try {
      $this->invalidator->invalidate($this->settings, $this->logger(), $paths, $tags);
      $this->setStates($invalidations, InvalidationInterface::SUCCEEDED);
    }
    catch (\Exception $e) {
      $this->logger()->error('%type: @message in %function (line %line of %file)', Error::decodeException($e));
      $this->setStates($invalidations, InvalidationInterface::FAILED);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasRuntimeMeasurement() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeHint() {
    return 4.0;
  }

  /**
   * Bulk updates invalidation states.
   *
   * @param \Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface[] $invalidations
   *   The invalidations.
   * @param int $state
   *   The invalidation state to set.
   */
  protected function setStates(array $invalidations, $state) {
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($state);
    }
  }

}
