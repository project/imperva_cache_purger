<?php

namespace Drupal\imperva_cache_purger\EventSubscriber;

use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\Core\Cache\CacheableResponseInterface;

/**
 * Response subscriber to send cache tags to imperva.
 */
class SendCacheTags implements EventSubscriberInterface {

  /**
   * Sets cache tags header on successful responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();

    if (!$response instanceof CacheableResponseInterface) {
      return;
    }
    $response_cacheability = $response->getCacheableMetadata();
    $response->headers->set('Cache-Tag', implode(',', $response_cacheability->getCacheTags()));
  }

  /**
   * Registers methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

}
