# IMPERVA cache purger

You can use IMPERVA as a reverse proxy in front of your whole Drupal site.

This module provides a **very simple** IMPERVA cache [Purge][1] **Purger** plugin.


## Installation

After enbling of module you need to add credentials for connection to you IMPERVA account.

Configuretion page you can find on /admin/config/development/performance/purge, you need
to click arrow near **Imperva cache Purger** and than click **Configure**. You will see
modal window where you need to set credentials for connection to you IMPERVA account.

## Configuration

Imperva support different type of invalidations: by path and by cache tags also. But we
recommend to use **by cache tag** as Drupal support it from the box. Information about
API ID or API key you can find on admin configuretion page or on Imperva
[documentation page][2].

## Testing

How to test purger plugin and process of invalidations, please read ducumentation of module
[purge][1].

[1]: https://www.drupal.org/project/purge
[2]: https://docs.imperva.com/bundle/cloud-application-security/page/settings/api-keys.htm